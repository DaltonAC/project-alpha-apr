from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.models import Task
from tasks.forms import CreateTask_Form


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTask_Form(request.POST)
        if form.is_valid():
            task = form.save()
            task.owner = request.user
            task.save()
            return redirect("list_projects")
    else:
        form = CreateTask_Form()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def list_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {"tasks": tasks}
    return render(request, "tasks/lists.html", context)


# @login_required
# def project_list(request):
#     projects = Project.objects.filter(owner=request.user)
#     context = {
#         "projects": projects,
#     }
#     return render(request, "projects/lists.html", context)
