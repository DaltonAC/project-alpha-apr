from django import forms


class LogIn_Form(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput(),
    )


class SignUp_Form(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput(),
    )
    password_confirmation = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput(),
    )
